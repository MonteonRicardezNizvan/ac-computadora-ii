# Arquitectura de computadoras

## Computadoras Electrónicas
El siguiente mapa conceptual abarca la evolución de la computadora, desde eléctrica hasta electrónica, así como la evolución de sus componentes principales.

```plantuml
@startmindmap
<style>
    node{
        LineColor coral
        BackgroundColor lightcyan
    }
    arrow{
        LineColor coral
    }
</style>
*[#lightcoral] COMPUTADORA\nELECTRÓNICA
** Máquina compuesta por\ncomponentes electrónicos,\ncapaz de realizar cálculos\nmatemáticos complejos
***[#lightblue] HISTORIA
**** En los años 40 las\ncomputadoras crecen\nen tamaño hasta\nocupar habitaciones\nenteras
*****_ se vuelve inevitable\nuna innovación
******[#lightblue] Harvard Mark I
******* Construida por\nIBM en 1944
******** Computadora electromecánica
********* 765,000 componentes
********* 80 km de cable
********* Eje de 15 metros
********* Motor de 5 caballos de fuerza
********_ uno de sus componentes\nfundamentales era el
*********[#lightgreen] RELÉ
********** Interruptor mecánico\ncontrolado eléctricamente
*********** Velocidad de 50 Hz\n(cambio de estado)
************_ esto solo le permitía\na Harvard Mark I hacer
************* 3 sumas o restas por segundo
************* Multiplicación en 6s
************* División en 15s
************* Operaciones complejas en minutos
*********** Posibilidad\nde avería
************ Harvard Mark I contaba con 3,500 relés
************* Por estadística fallaba un relé al día
********[#lightblue] USOS
********* Cálculo y simulaciones para el proyecto Manhattan
********_ por el calor que producía atraía a insectos
********* Surge el término bug para referirse a errores en las computadoras, al encontrar una polilla muerta en un relé
******[#lightblue] Era de las computa-\ndoras electrónicas
*******[#lightblue] Colossus\nMark I
******** Diseñada por Tommy\nFlowers en 1943
*********_ es conocida como
********** La primer computadora\nelectrónica programable
*********** Contaba con 1,600 tubos de vacío
*********** Se construyeron 10 Colossus
*********** La programacion de la misma se realizaba mediante la conexión de cientos de cables en un tablero
*********_ hacía uso de
**********[#lightgreen] TUBOS DE VACÍO
*********** Logran la misma funcionalidad\nde los relés
************ Cambian de estado miles de veces por segundo
************ Sin partes móviles, los relés se dañan menos con el uso continuo
************ Su principal desventaja es que son frágiles y se pueden quemar, como un foco
*********[#lightblue] USOS
********** Ayudaba a decodificar las comunicaciones Nazis
*******[#lightblue] ENIAC
******** Siglas de Calculadora\nIntegradora Numérica\nElectrónica
********* Primer computadora electrónica programable de propósito general
********** Podía realizar 5,000 sumas y restas de 10 dígitos por segundo
********* Diseñada por John Mauchly y J. Presper en 1946
********* Operó durante 10 años
********** Solamente era opreacional durante la mitad del día antes de dañarse
*******_ en 1947 surge el
********[#lightgreen] TRANSISTOR
********* Desarrollado por John\nBardeen, Walter Brattain\ny William Shockley en los\nlaboratorios Bell
********** Es un interruptor eléctrico
*********** Hechos principalmente de silicio
*********** Velocidad de 10,000 Hz
*********** Más resistentes y pequeños
**********_ hoy en día
*********** Tiene un tamaño menor a 50 nm
*********** Son resistentes y veloces
**********_ gracias a esto,\nse construyó
***********[#lightblue] IBM 608
************ lanzada\nen 1957
************* Primera computadora\ndisponible comercialmente\nbasada en transistores
**************_ podía\nrealizar
*************** 4,500 sumas por segundo
*************** 80 divisiones o multiplica-\nciones por segundo
@endmindmap
```

## Arquitectura Von Neumann y Arquitectura Harvard
En el siguiente mapa conceptual se desarrollan dos arquitecturas de la computación, la arquitectura Von Neumann y Harvard, tomando en cuenta los aspectos más importantes de cada una de ellas.

```plantuml
@startmindmap
<style>
    node{
        LineColor coral
        BackgroundColor lightcyan
    }
    arrow{
        LineColor coral
    }
</style>
*[#lightcoral] Arquitectura Von Neumann\ny Arquitectura Harvard
** Es la estructura operacional\nfundamental de un sistema
***[#lightblue] Ley de Moore
****_ esta ley establece que
***** La velocidad del procesador de las\ncomputadoras se duplica cada 12 meses
******_ en el apartado de
*******[#lightblue] ELECTRÓNICA
******** El número de transistores por cada chip se duplica cada año, pero el costo permanece sin cambios
******** Cada 18 meses se duplica la potencia de cálculo sin cambios en el costo
*******[#lightblue] RENDIMIENTO
********_ se incrementa
********* La velocidad del procesador
********* La velocidad de memoria
******** La velocidad de memoria siempre es menor que la del procesador
***[#lightblue] Funcionamiento básico\nde las computadoras
****[#lightblue] ANTES
***** Había sistemas cableados
****** La programación se hacía mediante el Hardware
*******_ su flujo de trabajo era
******** Ingreso de datos
******** Secuencia de funciones aritméticas/lógicas
******** Resultados
****[#lightblue] AHORA
***** La programación se hace mediante el Software
******_ su flujo de trabajo es
******* Ingreso de datos
******* Secuencia de funciones aritméticas/lógicas
******** Intérprete de instrucciones y señales de control
******* Resultados
***[#lightgreen] Arquitectura\nVon Neumann
**** Arquitectura moderna de\nlos ordenadores actuales
*****_ se compone de las
******[#lightblue] PARTES\nFUNDAMENTALES
******* Procesador o CPU
********_ contiene
********* Una Unidad de Control
********* Una Unidad Aritmética Lógica
********* Registro
******* Memoria principal de instrucciones y datos
******* Módulo de Entrada/Salida
******* Sistema de buses
******** Bus de control
********* Para las instrucciones
******** Bus de direcciones
********* Para direcciones de memoria
******** Bus de datos
********* Para el transporte de la información
*****_ está basada en la
****** Descrita por el matemático\nVon Neumann en 1945
******* Los datos y programas se almacenan en una única memoria de lectura/escritura
******* Se accede a la información de la memoria indicando su posición
******* Ejecución en secuencia
*****_ gracias a este modelo
****** Surge el concepto de programa almacenado
****** La separación de la memoria y la CPU genera el problema llamado\nNeumann bottleneck(también conocido como cuello de botella)
******* Debido a que la cantidad de datos que pasan entre la\nmemoria y el CPU difieren en las velocidades (throughput)
******** La CPU puede\npermanecer osciosa
*****[#lightblue] MODELO DE BUS
****** Existe un arbitraje para decidir que dispositivo hace uso del bus
****** Existen varios tipos de buses que conectan las distintas partes del computador
****** Refinamiento del modelo de Von Neumann
****** Su propósito es reducir la cantidad de conexiones
*****[#lightblue] FUNCIONAMIENTO
****** La función de una computadora es la ejecución\nde programas localizados en memoria
******* El procesador se encarga de ejecutar las\ninstrucciones a través del ciclo de instrucción
********_ se emplean
********* Lenguajes de bajo nivel,\ncomo ensamblador
********* Lenguajes de alto nivel,\ncomo Java o Python
*****[#lightblue] CICLOS
****** De ejecución
******* Fetch de instrucción
******** Obtiene la próxima instrucción
******* Decodificación de la instrucción
******* Fetch de operandos
******** Obtiene los operandos de la instrucción
******* Escritura de operandos
******* Ejecución de la instrucción
****** De una instrucción
******* Cálculo de la dirección de la instrucción
******* Captación de la instrucción
******* Decodificación de la operación
******* Cálculo de la dirección del operando (dato)
******** Captación del operando
******* Operación con el dato
******* Cálculo de la dirección del operando
******** Almacenamiento del operando
***[#lightgreen] Arquitectura Harvard
**** Originalmente se refiere a las\narquitecturas de computadoras\nque utilizaban dispositivos de\nalmacenamiento separados, uno\npara instrucciones y otro para datos
*****_ se compone de las
******[#lightblue] PARTES\nFUNDAMENTALES
******* Procesador o CPU
********_ contiene
********* Una Unidad de Control
********* Una Unidad Aritmética Lógica
********* Registro
******* Memoria
******** De instrucciones
********* Almacena las instrucciones del programa, utilizando memorias no volátiles\n(ROM, PROM, EPROM, EEPROM o flash)
******** De datos
********* Almacena los datos utilizados por los programas, habitualmente usando memoria\nSRAM, o si es necesario almacenar datos permanentemente, EEPROM o flash
******* Sistema de Entrada/Salida
******* Buses para cada memoria
******** De control
******** De dirección de instrucciones o de datos
******** De instrucciones o datos
*****[#lightblue] USO PRINCIPAL
****** Fabricar memorias mucho más\nrápidas es muy costoso, tal es el\ncaso de las memorias Cache
*******_ la arquitectura Harvard\nofrece una solución
******** Las instrucciones y los datos se\nalmacenan en caches separadas\npara mejor rendimiento
********* Suele utilizarse en PICs y\nMicrocontroladores utilizados\nen electrodométicos, procesamiento\nde audio y video
********* Funciona mejor cuando la frecuencia\nde lectura de instrucciones y datos\nes aproximadamente la misma
@endmindmap
```

## Basura Electrónica
En este mapa conceptual se abarca el tema de la basura electrónica, su impacto en el medioambiente y en la salud, así como la importancia del correcto tratamiento de la misma.

```plantuml
@startmindmap
<style>
    node{
        LineColor coral
        BackgroundColor lightcyan
    }
    arrow{
        LineColor coral
    }
</style>
*[#lightcoral] BASURA\nELECTRÓNICA
** Son todos los dispositivos electrónicos\no eléctricos cuya vida útil ha terminado\ny son desechados
***[#lightblue] TESORO EN LA BASURA
**** Se pueden extrar muchos materiales de valor de los residuos electrónicos
*****_ tales como
****** Estaño, Oro, Cobre, Plata, Cobalto, Acero
**** Es necesario reciclarlos de forma segura
***[#lightblue] RECICLAJE
**** Permite obtener los materiales de valor\ny hacer un adecuado tratamiento de los\ndesechos
*****_ se realiza de dos maneras
******[#lightgreen] LEGAL
******* Manera segura y adecuada de tratar los desechos electrónicos
********_ es un proceso
********* Costoso
********* Complicado
********* Seguro
******** Se utiliza equipo especializado para\nel reciclaje y el borrado de información
******[#coral] ILEGAL
******* Los desechos son importados de manera\nilegal y su tratamiento no es el adecuado
********_ es un proceso
********* Barato
********* Inseguro
********* Dañino para el medio ambiente
********* Dañino para la salud
******* Se derriten los materiales electrónicos para obtener el oro y otros recursos, el resto es desechado, contaminando\nagua y suelo, los vapores producidos durante el proceso son dañinos para la salud de los trabajadores
******* Se estima que uno de cada tres contenedores que sale de la Unión Europea contiene residuos electrónicos ilegales
***** Estos materiales pueden\nser reutilizados en\nfuturas fabricaciones
***[#lightblue] DATOS
**** Muchos de los desechos electrónicos contienen\ninformación de valor de sus anteriores dueños
***** La información personal o de empresas puede\nutilizarse para muchos fines criminales
****** Existen equipos para borrar los datos de discos duros o para\ndestruirlos, de esta manera la información queda a salvo
***[#lightblue] PROBLEMA\nAMBIENTAL
**** El creciente ritmo del desarrollo de nuevas\ntecnologías y cambio tecnológico generan\ncada vez más desechos electrónicos
***** Debido a la corta vida útil de los aparatos electrónicos
*****_ estos son los llamados
******[#lightblue] RAEE
******* Residuos de Aparatos\nEléctricos y Electrónicos
******** Deben ser tratados adecua-\ndamente para su reciclaje
********* Con estos se pueden\ngenerar materias\nprimas como
********** Plásticos
********** Vidrios
********** Electrónica
********** Metales
***** En México se genera un promedio de 300,000 toneladas de residuos electrónicos
***[#lightblue] PLANTAS DE\nRECICLAJE
**** Instalación en donde se procesan diferentes materiales, en este caso, residuos electrónicos, para poder ser reutilizados
*****_ algunos ejemplos de este tipo de plantas
****** e-end
******* Estados Unidos
****** Perú Green Recycling
******* Perú
****** Techemet
******* México
****** Servicios Ecológicos
******* Costa Rica
****** Remsa
******* México
***_ algunos ejemplos de\ndesechos electrónicos son
**** Computadoras, celulares, discos duros, audífonos, ratones y teclados, consolas de videojuegos, proyectores, teléfonos, etc. Cualquier aparato eléctrico que terminó su vida útil y es desechado
@endmindmap
```

---
Trabajo para la materia de Arquitectura de Computadoras.